<?php
/**
 * @file
 * western_gallery.default_breakpoint_group.inc
 */

/**
 * Implements hook_default_breakpoint_group().
 */
function western_gallery_default_breakpoint_group() {
  $export = array();

  $breakpoint_group = new stdClass();
  $breakpoint_group->disabled = FALSE; /* Edit this to true to make a default breakpoint_group disabled initially */
  $breakpoint_group->api_version = 1;
  $breakpoint_group->machine_name = 'wwuzen_western_gallery';
  $breakpoint_group->name = 'WWUZEN Western Gallery';
  $breakpoint_group->breakpoints = array(
    0 => 'breakpoints.theme.wwuzen_western_gallery.medium_large',
    1 => 'breakpoints.theme.wwuzen_western_gallery.large',
    2 => 'breakpoints.theme.wwuzen_western_gallery.medium',
    3 => 'breakpoints.theme.wwuzen_western_gallery.small',
  );
  $breakpoint_group->type = 'theme';
  $breakpoint_group->overridden = 0;
  $export['wwuzen_western_gallery'] = $breakpoint_group;

  return $export;
}
