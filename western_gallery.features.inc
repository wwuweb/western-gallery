<?php
/**
 * @file
 * western_gallery.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function western_gallery_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "breakpoints" && $api == "default_breakpoint_group") {
    return array("version" => "1");
  }
  if ($module == "breakpoints" && $api == "default_breakpoints") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function western_gallery_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function western_gallery_image_default_styles() {
  $styles = array();

  // Exported image style: 250x250.
  $styles['250x250'] = array(
    'label' => 'home-page-thumbnail',
    'effects' => array(
      5 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 210,
          'height' => 190,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: collection_image_style.
  $styles['collection_image_style'] = array(
    'label' => 'collection image style (188x188)',
    'effects' => array(
      6 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 188,
          'height' => 188,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: focal_thumb__100x100_.
  $styles['focal_thumb__100x100_'] = array(
    'label' => 'Focal Thumb (100x100)',
    'effects' => array(
      3 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 100,
          'height' => 100,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: header_image_100_.
  $styles['header_image_100_'] = array(
    'label' => 'header_image_100%',
    'effects' => array(),
  );

  // Exported image style: outdoor_sculpture__168x148_.
  $styles['outdoor_sculpture__168x148_'] = array(
    'label' => 'Outdoor Sculpture (168x148)',
    'effects' => array(
      4 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 168,
          'height' => 148,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: past_exhibit_image.
  $styles['past_exhibit_image'] = array(
    'label' => 'past exhibit image',
    'effects' => array(
      7 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 275,
          'height' => 235,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function western_gallery_node_info() {
  $items = array(
    'collection' => array(
      'name' => t('Collection'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'exhibition' => array(
      'name' => t('Exhibition'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'outdoor_sculpture' => array(
      'name' => t('Outdoor Sculpture'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Outdoor Sculpture'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
