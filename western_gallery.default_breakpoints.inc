<?php
/**
 * @file
 * western_gallery.default_breakpoints.inc
 */

/**
 * Implements hook_default_breakpoints().
 */
function western_gallery_default_breakpoints() {
  $export = array();

  $breakpoint = new stdClass();
  $breakpoint->disabled = FALSE; /* Edit this to true to make a default breakpoint disabled initially */
  $breakpoint->api_version = 1;
  $breakpoint->machine_name = 'breakpoints.theme.wwuzen_western_gallery.large';
  $breakpoint->name = 'large';
  $breakpoint->breakpoint = '(min-width: 801px)';
  $breakpoint->source = 'wwuzen_western_gallery';
  $breakpoint->source_type = 'theme';
  $breakpoint->status = 1;
  $breakpoint->weight = 1;
  $breakpoint->multipliers = array(
    '1x' => '1x',
  );
  $export['breakpoints.theme.wwuzen_western_gallery.large'] = $breakpoint;

  $breakpoint = new stdClass();
  $breakpoint->disabled = FALSE; /* Edit this to true to make a default breakpoint disabled initially */
  $breakpoint->api_version = 1;
  $breakpoint->machine_name = 'breakpoints.theme.wwuzen_western_gallery.medium';
  $breakpoint->name = 'medium';
  $breakpoint->breakpoint = '(max-width: 800px) and (min-width: 551px)';
  $breakpoint->source = 'wwuzen_western_gallery';
  $breakpoint->source_type = 'theme';
  $breakpoint->status = 1;
  $breakpoint->weight = 2;
  $breakpoint->multipliers = array(
    '1x' => '1x',
  );
  $export['breakpoints.theme.wwuzen_western_gallery.medium'] = $breakpoint;

  $breakpoint = new stdClass();
  $breakpoint->disabled = FALSE; /* Edit this to true to make a default breakpoint disabled initially */
  $breakpoint->api_version = 1;
  $breakpoint->machine_name = 'breakpoints.theme.wwuzen_western_gallery.medium_large';
  $breakpoint->name = 'medium_large';
  $breakpoint->breakpoint = '(min-width: 801px) and (max-width: 975px)';
  $breakpoint->source = 'wwuzen_western_gallery';
  $breakpoint->source_type = 'theme';
  $breakpoint->status = 1;
  $breakpoint->weight = 0;
  $breakpoint->multipliers = array(
    '1x' => '1x',
  );
  $export['breakpoints.theme.wwuzen_western_gallery.medium_large'] = $breakpoint;

  $breakpoint = new stdClass();
  $breakpoint->disabled = FALSE; /* Edit this to true to make a default breakpoint disabled initially */
  $breakpoint->api_version = 1;
  $breakpoint->machine_name = 'breakpoints.theme.wwuzen_western_gallery.small';
  $breakpoint->name = 'small';
  $breakpoint->breakpoint = '(max-width: 550px)';
  $breakpoint->source = 'wwuzen_western_gallery';
  $breakpoint->source_type = 'theme';
  $breakpoint->status = 1;
  $breakpoint->weight = 3;
  $breakpoint->multipliers = array(
    '1x' => '1x',
  );
  $export['breakpoints.theme.wwuzen_western_gallery.small'] = $breakpoint;

  return $export;
}
